﻿using System;
using System.Reactive;
using Avalonia.Controls;
using ReactiveUI;
using SistemaEducativo.Windows.Interfaces;

namespace SistemaEducativo.Windows.ViewModels
{
    internal abstract class ApplicationModelBase : ReactiveObject
    {
        private readonly IThemeSwitch _themeSwitch;

        private Boolean _aboutEnable;
        private Boolean _defaultLightEnable;
        private Boolean _defaultDarkEnable;
        private Boolean _fluentLightEnable;
        private Boolean _fluentDarkEnable;

        public Boolean AboutEnabled
        {
            get => _aboutEnable;
            set => this.RaiseAndSetIfChanged(ref _aboutEnable, value);
        }

        public Boolean DefaultLightEnabled
        {
            get => _defaultLightEnable;
            set => this.RaiseAndSetIfChanged(ref _defaultLightEnable, value);
        }

        public Boolean DefaultDarkEnabled
        {
            get => _defaultDarkEnable;
            set => this.RaiseAndSetIfChanged(ref _defaultDarkEnable, value);
        }

        public Boolean FluentLightEnabled
        {
            get => _fluentLightEnable;
            set => this.RaiseAndSetIfChanged(ref _fluentLightEnable, value);
        }

        public Boolean FluentDarkEnabled
        {
            get => _fluentDarkEnable;
            set => this.RaiseAndSetIfChanged(ref _fluentDarkEnable, value);
        }

        public ReactiveCommand<Unit, Unit> FileExitCommand { get; }

        protected ApplicationModelBase(IThemeSwitch themeSwitch)
        {
            _themeSwitch = themeSwitch;
            IntializeTheme(themeSwitch.Current);
            FileExitCommand = ReactiveCommand.Create(RunFileExit);

            // Se inicializan las variables booleanas aquí
            _aboutEnable = true;
            _defaultLightEnable = false;
            _defaultDarkEnable = false;
            _fluentLightEnable = false;
            _fluentDarkEnable = false;
        }

        public abstract void HelpAboutMethod();
        public abstract void DefaultLightMethod();
        public abstract void DefaultDarkMethod();
        public abstract void FluentLightMethod();
        public abstract void FluentDarkMethod();

        protected async void RunHelpAbout(IMainWindow currentWindow)
        {
            if (AboutEnabled)
            {
                try
                {
                    AboutEnabled = false;
                    //await new AboutWindow(IsDarkTheme(_themeSwitch.Current)).ShowDialog(currentWindow as Window);
                    var window = currentWindow as Window;
                    if (window != null)
                    {
                        await new AboutWindow(IsDarkTheme(_themeSwitch.Current)).ShowDialog(window);
                    }

                    else
                    {
                        // Lanza una excepción cuando currentWindow no es una instancia de Window
                        throw new InvalidOperationException("currentWindow debe ser una instancia de Window");
                    }
                }
                finally

                {
                    AboutEnabled = true;
                }
            }
        }

        protected void SetTheme(ApplicationTheme theme)
        {
            IntializeTheme(theme);
            _themeSwitch.ChangeTheme(theme);
        }

        private void RunFileExit() => Environment.Exit(0);

        private void IntializeTheme(ApplicationTheme theme)
        {
            DefaultLightEnabled = theme != ApplicationTheme.SimpleLight;
            DefaultDarkEnabled = theme != ApplicationTheme.SimpleDark;
            FluentLightEnabled = theme != ApplicationTheme.FluentLight;
            FluentDarkEnabled = theme != ApplicationTheme.FluentDark;
        }

        private static Boolean IsDarkTheme(ApplicationTheme? theme)
            => theme switch
            {
                ApplicationTheme.SimpleDark => true,
                ApplicationTheme.FluentDark => true,
                _ => false,
            };
    }
}