﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using Avalonia.Media;

using ReactiveUI;

namespace SistemaEducativo.Windows.ViewModels
{
    internal record SystemDetail(string Key, string Value);

    internal sealed class AboutViewModel : ReactiveObject
    {
        private readonly IImage _computerImage;
        private readonly Boolean _darkTheme;

        public IImage ComputerImage => _computerImage;

        public IReadOnlyList<SystemDetail> SystemDetails { get; } = new[]
        {
            new SystemDetail("Número de núcleos", Environment.ProcessorCount.ToString()),
            new SystemDetail("SO", RuntimeInformation.OSDescription),
            new SystemDetail("Arquitectura del SO", RuntimeInformation.OSArchitecture.ToString()),
            new SystemDetail("Versión del SO", Environment.OSVersion.ToString()),
            new SystemDetail("Computadora", Environment.MachineName),
            new SystemDetail("Usuario", Environment.UserName),
            new SystemDetail("Ruta del sistema", Environment.SystemDirectory),
            new SystemDetail("Ruta actual", Environment.CurrentDirectory),
            new SystemDetail("Arquitectura del proceso", RuntimeInformation.ProcessArchitecture.ToString()),
            new SystemDetail("Nombre del tiempo de ejecución", RuntimeInformation.FrameworkDescription),
            new SystemDetail("Ruta del tiempo de ejecución", RuntimeEnvironment.GetRuntimeDirectory()),
            new SystemDetail("Versión del tiempo de ejecución", RuntimeEnvironment.GetSystemVersion()),
            new SystemDetail("Versión del framework", Environment.Version.ToString()),
        };


        private String ComputerImageName
        {
            get
            {
                if (Utilities.IsWindows)
                    return !this._darkTheme ? "windows.png" : "windows_d.png";
                else if (Utilities.IsOsx)
                    return !this._darkTheme ? "macos.png" : "macos_d.png";
                else
                    return !this._darkTheme ? "linux.png" : "linux_d.png";
            }
        }

        public AboutViewModel(Boolean darkTheme)
        {
            this._darkTheme = darkTheme;
            this._computerImage = Utilities.GetImageFromResources(this.ComputerImageName);
        }

        ~AboutViewModel()
        {
            (this._computerImage as IDisposable)?.Dispose();
        }
    }
}
