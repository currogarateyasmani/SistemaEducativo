﻿using SistemaEducativo.Windows.Interfaces;

namespace SistemaEducativo.Windows.ViewModels
{
    internal sealed class MainViewModel : ApplicationModelBase
    {
        private readonly IMainWindow _window;

        public MainViewModel(IMainWindow window, IThemeSwitch themeSwitch)
            : base(themeSwitch)
        {
            this._window = window;
        }

        //public override void HelpAboutMethod() => base.RunHelpAbout(this._window);
        //public override void DefaultLightMethod() => base.SetTheme(ApplicationTheme.SimpleLight);
        //public override void DefaultDarkMethod() => base.SetTheme(ApplicationTheme.SimpleDark);
        //public override void FluentLightMethod() => base.SetTheme(ApplicationTheme.FluentLight);
        //public override void FluentDarkMethod() => base.SetTheme(ApplicationTheme.FluentDark);

        public override void HelpAboutMethod() => RunHelpAbout(this._window);
        public override void DefaultLightMethod() => SetTheme(ApplicationTheme.SimpleLight);
        public override void DefaultDarkMethod() => SetTheme(ApplicationTheme.SimpleDark);
        public override void FluentLightMethod() => SetTheme(ApplicationTheme.FluentLight);
        public override void FluentDarkMethod() => SetTheme(ApplicationTheme.FluentDark);
        
    }
}