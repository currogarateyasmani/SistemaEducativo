﻿using System;
using Avalonia.Media;

namespace SistemaEducativo.Windows.Interfaces
{
    public interface IMainWindowState
    {
        IImage DotNetImage { get; }
        IImage AvaloniaImage { get; }
        String? Text { get; }

        void SetUnloadable();
    }
}

