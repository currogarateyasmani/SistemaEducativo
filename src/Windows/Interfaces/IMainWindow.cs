﻿using System;

using Avalonia;
using Avalonia.Controls;

namespace SistemaEducativo.Windows.Interfaces;

public interface IMainWindow
{
    ApplicationTheme CurrentTheme { get; }
    void ChangeTheme(ApplicationTheme theme);
    IThemeSwitch ThemeSwitch { get; }
    IMainWindowState Model { get; }
    PixelPoint Position { get; }
    Size ClientSize { get; }
    Size? FrameSize { get; }
    WindowState State { get; }
    Double Height { get; }
    Double Width { get; }
}