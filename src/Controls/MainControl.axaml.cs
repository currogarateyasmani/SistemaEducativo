using Avalonia.Controls;

using SistemaEducativo.Controls.ViewModels;
using SistemaEducativo.Windows.Interfaces;

namespace SistemaEducativo.Controls
{
    public sealed partial class MainControl : UserControl
    {
        public MainControl()
        {
            this.InitializeComponent();
        }

        private void InitializeComponent()
        {
            //Use generated InitializeComponent method.
            this.InitializeComponent(loadXaml: true);
            this.DataContext = new MainControlViewModel();
        }

        public void Reload(IMainWindowState model)
        {
            this.DataContext = new MainControlViewModel(model);
        }
    }
}
