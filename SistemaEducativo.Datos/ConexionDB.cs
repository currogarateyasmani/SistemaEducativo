﻿using System.Data.SqlClient;
using SistemaEducativo.Datos.Interfaces;

namespace SistemaEducativo.Datos
{
    public class ConexionDb : IConexionDb
    {
        private readonly string Base = "menu_dinamico";
        private readonly string Servidor = "DESKTOP-8LA7DUM";
        private readonly string Usuario = "sa";
        private readonly string Clave = "admin";
        private readonly bool Seguridad = true;

        // Inicializa instance aquí
        private static ConexionDb instance = new ConexionDb();

        private ConexionDb()
        {
        }

        public static IConexionDb GetInstance()
        {
            return instance;
        }

        public SqlConnection CrearConexion()
        {
            try
            {
                var connectionStringBuilder = new SqlConnectionStringBuilder
                {
                    DataSource = Servidor,
                    InitialCatalog = Base,
                    IntegratedSecurity = Seguridad
                };

                if (!Seguridad)
                {
                    connectionStringBuilder.UserID = Usuario;
                    connectionStringBuilder.Password = Clave;
                }

                return new SqlConnection(connectionStringBuilder.ConnectionString);
            }
            catch (SqlException ex)
            {
                // Aquí podrías manejar las excepciones específicas de SQL Server
                throw new Exception("Error al crear la conexión a la base de datos.", ex);
            }
            catch (Exception ex)
            {
                // Otras excepciones generales
                throw new Exception("Error general al crear la conexión a la base de datos.", ex);
            }
        }
    }
}
