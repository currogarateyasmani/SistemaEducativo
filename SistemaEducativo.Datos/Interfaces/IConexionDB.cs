﻿using System.Data.SqlClient;

namespace SistemaEducativo.Datos.Interfaces
{
    public interface IConexionDb
    {
        SqlConnection CrearConexion();
    }
}