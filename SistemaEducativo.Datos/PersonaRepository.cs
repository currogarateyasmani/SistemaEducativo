﻿using SistemaEducativo.Datos.Interfaces;
using SistemaEducativo.Entidades;
using System.Data;
using System.Data.SqlClient;

namespace SistemaEducativo.Datos
{
    public class PersonaRepository : IPersonaRepository
    {
        private readonly IConexionDb _conexionDB;

        public PersonaRepository(IConexionDb conexionDB)
        {
            _conexionDB = conexionDB ?? throw new ArgumentNullException(nameof(conexionDB));
        }

        public Persona ObtenerPorDni(string dni)
        {
            using (var conexion = _conexionDB.CrearConexion())
            {
                var query = "SELECT * FROM Persona WHERE dni = @dni";
                using (var comando = new SqlCommand(query, conexion))
                {
                    comando.Parameters.AddWithValue("@dni", dni);
                    conexion.Open();
                    using (var reader = comando.ExecuteReader())
                    {
                        return reader.Read() ? MapPersonaDesdeReader(reader) : null;
                    }
                }
            }
        }

        public IEnumerable<Persona> ObtenerTodas()
        {
            var personas = new List<Persona>();
            using (var conexion = _conexionDB.CrearConexion())
            {
                var query = "SELECT * FROM Persona";
                using (var comando = new SqlCommand(query, conexion))
                {
                    conexion.Open();
                    using (var reader = comando.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            personas.Add(MapPersonaDesdeReader(reader));
                        }
                    }
                }
            }
            return personas;
        }

        public void Insertar(Persona persona)
        {
            using (var conexion = _conexionDB.CrearConexion())
            {
                var query = @"INSERT INTO Persona (dni, estado, ruc, apellidos_paterno, apellido_materno, nombres, edad, sexo, foto, email)
                              VALUES (@dni, @estado, @ruc, @apellidosPaterno, @apellidoMaterno, @nombres, @edad, @sexo, @foto, @email)";
                using (var comando = new SqlCommand(query, conexion))
                {
                    ConfigurarParametros(comando, persona);
                    conexion.Open();
                    comando.ExecuteNonQuery();
                }
            }
        }

        public void Actualizar(Persona persona)
        {
            using (var conexion = _conexionDB.CrearConexion())
            {
                var query = @"UPDATE Persona SET estado = @estado, ruc = @ruc, apellidos_paterno = @apellidosPaterno, 
                              apellido_materno = @apellidoMaterno, nombres = @nombres, edad = @edad, sexo = @sexo, 
                              foto = @foto, email = @email
                              WHERE dni = @dni";
                using (var comando = new SqlCommand(query, conexion))
                {
                    ConfigurarParametros(comando, persona);
                    conexion.Open();
                    comando.ExecuteNonQuery();
                }
            }
        }

        public void Eliminar(string dni)
        {
            using (var conexion = _conexionDB.CrearConexion())
            {
                var query = "DELETE FROM Persona WHERE dni = @dni";
                using (var comando = new SqlCommand(query, conexion))
                {
                    comando.Parameters.AddWithValue("@dni", dni);
                    conexion.Open();
                    comando.ExecuteNonQuery();
                }
            }
        }

        private Persona MapPersonaDesdeReader(IDataReader reader)
        {
            return new Persona
            {
                Dni = reader["dni"].ToString(),
                Estado = Convert.ToBoolean(reader["estado"]),
                Ruc = reader["ruc"].ToString(),
                ApellidosPaterno = reader["apellidos_paterno"].ToString(),
                ApellidoMaterno = reader["apellido_materno"].ToString(),
                Nombres = reader["nombres"].ToString(),
                Edad = Convert.ToInt32(reader["edad"]),
                Sexo = Convert.ToChar(reader["sexo"]),
                Foto = reader["foto"].ToString(),
                Email = reader["email"].ToString()
            };
        }

        private void ConfigurarParametros(SqlCommand comando, Persona persona)
        {
            comando.Parameters.AddWithValue("@dni", persona.Dni);
            comando.Parameters.AddWithValue("@estado", persona.Estado);
            comando.Parameters.AddWithValue("@ruc", persona.Ruc);
            comando.Parameters.AddWithValue("@apellidosPaterno", persona.ApellidosPaterno);
            comando.Parameters.AddWithValue("@apellidoMaterno", persona.ApellidoMaterno);
            comando.Parameters.AddWithValue("@nombres", persona.Nombres);
            comando.Parameters.AddWithValue("@edad", persona.Edad);
            comando.Parameters.AddWithValue("@sexo", persona.Sexo);
            comando.Parameters.AddWithValue("@foto", persona.Foto);
            comando.Parameters.AddWithValue("@email", persona.Email);
        }
    }
}